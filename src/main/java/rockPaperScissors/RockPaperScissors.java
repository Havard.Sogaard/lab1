package rockPaperScissors;

import java.util.List;
import java.util.Scanner;
import java.util.Random; 
import java.util.ArrayList;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
    	while(true) {
    		System.out.println("Let's play round " + roundCounter);
    		
    		roundCounter++;
    	
    		//System.out.println("Etter stepping " + roundCounter);//debug
    	
    		// Generate the computers move
    	
    		int computerPlay = computerMove();
    	
    		//System.out.println(computerPlay); //debug
    	
    		int userPlay = readInput("Your choice (Rock/Paper/Scissors)?");
    		//System.out.println(userPlay); //debug
    	
    		List<Integer> l1 = new ArrayList<Integer>();
    	 
    		// Adding elements to object of List interface
    		// Custom inputs
 
    		l1.add(0, userPlay);
    		l1.add(1, computerPlay);
        
    		int whoWon = resultOut(l1);
        
    		//System.out.println(resultOut(l1));//debug
        
    		if (whoWon == 1) {
    			humanScore++;
    		}
    		else if (whoWon == 2) {
    			computerScore++;
    		}
    		System.out.println("Score: human " + humanScore + ", computer " + computerScore +"\nDo you wish to continue playing? (y/n)?");
    		String userInput = sc.next();
    		if (userInput.equals("n")) {
    			System.out.println("Bye bye :)");
    			break;
    		}	
    	}
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public int readInput(String prompt) {
    	int userPlay = 0;
    	
    	while(true){
    	
    		System.out.println(prompt);
    		String userInput = sc.next();
        
    		if (userInput.equals("rock")) { 
    			userPlay = 1;
    			break;
    		}
    		else if (userInput.equals("paper")) {
    			userPlay = 2;
    			break;
    		}
    		else if (userInput.equals("scissors")) {
    			userPlay = 3;
    			break;
    		}
    		else {
    			System.out.println("I do not understand " + userInput + ". Could you try again?");
    		}
    	}
        return userPlay;
    }
    
    public int computerMove() {
    	int computerInt;
    	Random generator = new Random();
    	computerInt = generator.nextInt(3)+1;
    	//System.out.println(computerInt); //debug  	
        
    	return computerInt;
    }
    
    public int resultOut(List<Integer> moves) {
    	
    	int result = 0;
    	
    	if (moves.get(0).equals(1)) {
     	   if (moves.get(1).equals(3)) { 
     		  System.out.println("Human chose rock, computer chose scissors. Human wins!");
     		  result = 1;
     	   }
     	   else if (moves.get(1).equals(2)) {
     		  System.out.println("Human chose rock, computer chose paper. Computer wins!");
     	      result = 2;
     	   }
     	   else if (moves.get(1).equals(1)) {
     		  System.out.println("Human chose rock, computer chose rock. It's a tie!");
     	   }
     	}
     	else if (moves.get(0).equals(2)) {
     	   if (moves.get(1).equals(3)) {
     		  System.out.println("Human chose paper, computer chose scissors. Computer wins!");
     		  result = 2;
     	   }
     	   else if (moves.get(1).equals(2)) {
     		  System.out.println("Human chose paper, computer chose paper. It's a tie!");
     	   }
     	   else if (moves.get(1).equals(1)) {
     		  System.out.println("Human chose paper, computer chose rock. Human wins!"); 
     		  result = 1;
     	   }
     	}
     	else if (moves.get(0).equals(3)) {
     		if (moves.get(1).equals(3)) {
     			System.out.println("Human chose scissors, computer chose scissors. It's a tie!");
     		}
     		else if (moves.get(1).equals(2)) {
     			System.out.println("Human chose scissors, computer chose paper. Human wins!");
     			result = 1;
     		}
     		else if (moves.get(1).equals(1)) {
     			System.out.println("Human chose scissors, computer chose rock. Computer wins!");
     			result = 2;
     		}
     	}
    	return result;
    }
}
